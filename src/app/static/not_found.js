import React from 'react';
import notFound from '../../media/not_found.jpg';


export class NotFound extends React.Component {

  render() {
    return (
      <section>
        <div className="not-found">
          <h1>Lo sentimos mucho, esta propiedad no existe</h1>
          <img src={notFound} alt="gif" />
        </div>
      </section>
    );
  }
}

export default NotFound;
