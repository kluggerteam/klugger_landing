import React from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router';
import logo from '../../media/logo-static.svg';


export class PoliticaPrivacidad extends React.Component {
  componentDidMount(){
    ReactDOM.findDOMNode(document.body).scrollIntoView();
  }
  render() {
    return (
      <section className="background-static">
        <Link to="/">
          <img src={logo} alt="logo" />
        </Link>
        <hr />
        <section className="static-pages">

          <h1>Políticas de privacidad</h1>
          <p>En cumplimiento con lo establecido por la Ley Federal de Protección de Datos Personales en Posesión de Particulares, “Servicios de Consultoría en inteligencia de datos S.A de C.V. (en adelante "Klugger") con domicilio en calle Holbein 517 int 503 Colonia Nochebuena Delegación Benito Juárez C.P.03720, México, Distrito Federal, hace de su conocimiento la política de privacidad y manejo de datos personales, en la que en todo momento buscará que el tratamiento de los mismos sea legítimo, controlado e informado, a efecto de garantizar la privacidad de los mismos. "Klugger" , a través de www.klugger.mx (en adelante el “Portal”), tiene entre otros objetivos, la convicción de proteger los datos personales, proporcionados por los usuarios; así como los principios de licitud, consentimiento, información, calidad, finalidad, lealtad, proporcionalidad y responsabilidad en el tratamiento de los mismos (en adelante, conjuntamente la “Información”). . Por lo anterior,"Klugger" ha establecido las siguientes disposiciones para proteger dicha Información en la medida de lo posible, las cuales pueden cambiar en cualquier momento. Por lo que "Klugger" sugiere consultarlas periódicamente. Los términos que no estén expresamente definidos en estas disposiciones, tendrán el significado que se les atribuye a los mismos en los “Términos y Condiciones para el uso del portal.”</p>
          <b>ACEPTACIÓN DE LOS TÉRMINOS</b>
          <p>Esta declaración de Confidencialidad / Privacidad está sujeta a los términos y condiciones de www.klugger.mx, lo cual constituye un acuerdo legal entre el usuario y Klugger.
          Si el usuario utiliza nuestros servicios, significa que ha leído, entendido y acordado los términos antes expuestos.</p>

          <b>1. INFORMACIÓN PERSONAL SOLICITADA.</b>
          <p>Los datos personales que proporcione cualquier usuario, incluyendo sin limitar, suscriptores, clientes, proveedores y/o usuarios, en favor de "Klugger" tendrán el uso que se describe a continuación, pudiendo ampliarse en un futuro,siempre comunicándolo a través de nuestra política de protección de datos y privacidad:</p>
          <p>- Para identificarle, ubicarle, comunicarle, contactarle o enviarle información, así como para su transmisión a terceros por cualquier medio. El uso de los datos personales tendrá relación con el tipo de interacción que el usuario tiene con
          "Klugger" en su calidad de responsable de la Información, ya sea que la relación entre el usuario y "Klugger" sea de naturaleza comercial, civil, mercantil o de cualquier otra naturaleza.</p>
          <p>- Al visitar el Portal, “Mercado i” recopila automáticamente algunos datos personales, que luego suma a un conjunto para que no estén asociados a una única persona que sea identificable. Con esta información se elaboran estadísticas sobre cuántos usuarios visitan el Portal y a qué páginas accedieron. Al recopilar esta Información se adaptan de la mejor manera a los sitios web a los Usuarios y Visitantes. Tal y como adelante se señala, "Klugger"recopila esta Información a través de "archivos temporales" o "web beacons".</p>

          <p>- Archivos de registro: Los archivos temporales son archivos web del servidor (como el nombre del dominio o la direc-
          ción IP, URL, el código de respuesta http, el sitio web desde el que nos ha visitado o la fecha y duración de su visita) que se crean automáticamente cuando un usuario de Internet visita un sitio. Una vez que el Usuario deja de usar el
          sitio web, se procesa y utiliza esta información sólo para permitir más conexiones, para temas de facturación, para detectar interrupciones en el equipo de telecomunicaciones de "Klugger", así como para detectar abusos en los servi-
          cios de telecomunicaciones. Esta información se elimina inmediatamente una vez finalizada la coanera más satisfactoria los servicios, o bien cuando los usuarios quieran hacer una compra de cualquiera de los productos o servicios ofrecidos en el Portal. Para efectos de lo anterior, "Klugger" invariablemente le solicitará al usuario que inicie sesión con una dirección de correo electrónico y una contraseña. Los usuarios de los servicios deberán otorgar su consentimiento
          libre y voluntariamente para proporcionar la Información que le sea requerida, en el entendido de que si el usuario optara por no proporcionar la información obligatoria, dicho usuario no podrá acceder a los servicios para los cuales le
          es requerida.</p>
          <p>Los datos que solicitaremos para crear un Usuario del Sitio son los siguientes: Nombre; Apellido; correo electrónico; teléfono; teléfono móvil; Provincia; Ciudad; Zona.- "Klugger" no recabara ni solicitara datos sensibles.</p>

          <b>2. USO, FINALIDAD Y TRATAMIENTO DE LA INFORMACIÓN.</b>
          <p>"Klugger" utiliza la tecnología más avanzada a su alcance para la protección de la Información proporcionada por los usuarios del Portal.
          "Klugger" podrá transmitir la Información proporcionada por sus usuarios a terceros que tengan cualquier relación jurídica con aquél, ya sea a nivel nacional o internacional. "klugger" también recolectará información que es derivada de los gustos, preferencias y en general de la utilización que hacen los usuarios del Portal y de sus servicios. Dicha información derivada, al igual que la Información que los usuarios proporcionen, podrá ser utilizada para diversos objetivos comerciales, como lo es el proporcionar datos estadísticos, enviar publicidad a los usuarios de acuerdo a sus intereses específicos, conducir investigaciones de mercadeo, y otras actividades o promociones que "Klugger" considere apropiadas. "Klugger" también podrá revelar Información cuando por mandato de ley y/o de autoridad competente le fuere requerido, o por considerar de buena fe que dicha revelación es necesaria para: i) Cumplir con procesos legales; ii) Cumplir con el convenio del usuario; iii) Responder reclamaciones que involucren cualquier contenido que menoscabe derechos de terceros, o; iv) Proteger los derechos, la propiedad o la seguridad de "Klugger", sus usuarios y el público en general.
          "Klugger" se reserva el derecho de ceder la Información proporcionada por los usuarios a cualquiera de sus filiales o subsidiarias, ya sean presentes, o aquellas que se constituyan o adquieran en el futuro, tanto a nivel nacional, como
          internacional.</p>

          <b>3. PROTECCIÓN DE LA INFORMACIÓN.</b>
          <p>"Klugger" expresa su compromiso de proteger la seguridad de la Información proporcionada por los usuarios. Para dichos fines, utiliza una gran variedad de tecnologías y procedimientos de seguridad, para impedir, en la medida de lo posible, el acceso, uso o divulgación no autorizados. Cuando se transmite información altamente confidencial relacionada con el pago de servicios (como número de tarjetas bancarias) se hace a través de portales especializados como
          Paypal y dineromail, por lo que "IKlugger" no es responsable en forma alguna por la protección de dicha información.</p>

          <p>"Klugger" ha implementado medidas apropiadas de carácter técnico y organizativo que están diseñadas para garantizar la seguridad de su información personal frente a pérdidas accidentales y frente al acceso, uso, alteración o divulgación no autorizados. En particular, todos los datos de la cuenta y de la clave de acceso están encriptados (es decir, secodifican entre su computadora y los servidores de "Klugger"). Pese a estas medidas, Internet es un sistema abierto y no podemos garantizar que terceras partes no autorizadas nunca podrán romper estas medidas o usar su información personal para fines indebidos.</p>

          <b>4. COOKIES.</b>
          <p>El usuario reconoce y acepta que "Klugger" podrá utilizar un sistema de seguimiento mediante la utilización de cookies (las "Cookies"). Las Cookies son pequeños archivos que se instalan en el disco rígido, con una duración limitada en el tiempo que ayudan a personalizar los servicios."Klugger" también ofrece ciertas funcionalidades que sólo están disponibles mediante el empleo de Cookies. Las Cookies se utilizan con el fin de conocer los intereses, el comportamiento y la demografía de quienes visitan o son usuarios del Portal y de esa forma, comprender mejor sus necesidades e intereses y darles un mejor servicio o proveerle información relacionada. "Klugger" podrá usar la información obtenida por intermedio de las Cookies para analizar las páginas navegadas por el visitante o usuario, las búsquedas realizadas, mejorar sus iniciativas comerciales y promocionales, mostrar publicidad o promociones, banners de interés, noticias sobre "Klugger", perfeccionar la oferta de contenidos y artículos, personalizar dichos contenidos, presentación y servicios; "Klugger" podrá utilizar Cookies para promover y hacer cumplir las reglas y seguridad del Portal. "Klugger" también podrá agregar Cookies en los correos electrónicos que envíe para medir la efectividad de las promociones. "Klugger" utilizará adicionalmente las Cookies para que el usuario no tenga que introducir su clave tan frecuentemente
          durante una sesión de navegación, también para contabilizar y corroborar las inscripciones, la actividad del usuario y otros conceptos para los diversos programas y otros acuerdos comerciales, siempre teniendo como objetivo de la instalación de las Cookies, el beneficio del usuario que la recibe, y no será usado con otros fines ajenos a "Klugger".</p>
          <p>Se establece que la instalación, permanencia y existencia de las Cookies en el computador del usuario depende de su exclusiva voluntad y puede ser eliminada de su computador cuando el usuario así lo desee. Para saber cómo quitar las
          Cookies del Sistema es necesario revisar la sección Ayuda (Help) del navegador.
          El Portal usa diversas “cookies” para iniciar sesión, y así el usuario pueda utilizar algunos de los servicios de "Klugger".
          Las “cookies” contienen información, que posteriormente puede leer un servidor web con el objetivo de almacenar sus preferencias y otro tipo de información en el equipo, para que de este modo, ahorrar tiempo al usuario, al eliminar la
          necesidad de escribir varias veces la misma Información, además permite mostrar contenidos personalizados.</p>

          <b>5. ACCESO, RECTIFICACIÓN, CANCELACIÓN U OPOSICIÓN DEL USO DE INFORMACIÓN.</b>
          <p>Los usuarios tienen reconocidos y podrán ejercitar los derechos de acceder, rectificar, cancelar y actualizar su Información, incluyendo su dirección de correo electrónico, así como oponerse al tratamiento de la Información misma y a ser
          informado de las cesiones llevadas a cabo, todo ello de conformidad a lo dispuesto en la legislación aplicable. Los usuarios garantizan y responden, en cualquier caso, de la veracidad, exactitud, vigencia y autenticidad de la Información facilitada, y se comprometen a mantenerla debidamente actualizada.</p>
          <p>Una vez inscrito en el Portal, el usuario podrá revisar y cambiar la Información que ha enviado durante el proceso de inscripción incluyendo:</p>
          <p>- Dirección de correo electrónico. Sin perjuicio de los cambios que realice, "Klugger" conservará la Información Personal anterior por motivos de seguridad y control del fraude.</p>
          <p>- La información de la inscripción como: Usuario, en su caso compañía, domicilio, ciudad, región, código postal, país, número principal de teléfono, número secundario de teléfono, número de fax, correo electrónico, etc.</p>
          <p>- La clave de acceso al Portal.</p>
          <p>En determinados casos, "Klugger" a su propio criterio mantendrá en sus archivos la Información que el usuario haya solicitado fuera retirada, con la finalidad de resolver disputas o reclamaciones, detectar problemas o incidencias y solucionarlos, y dar cumplimiento a lo dispuesto en los Términos y Condiciones Generales por un período de tiempo determinado por la ley. En cualquier caso, la Información Personal de un usuario no será inmediatamente retirada de los archivos de "Klugger" por motivos legales y técnicos, incluyendo sistemas de soportes de seguridad. Por tanto, no se deba esperar que toda la Información sea definitivamente borrada de las bases de datos de "Klugger".
          Los usuarios deben actualizar su Información periódicamente conforme la misma sea modificada para que los otros usuarios puedan ubicarlos cuando realicen una operación. Para hacer cualquier modificación en la Información que es
          suministrada en el momento de la Inscripción, se debe ingresar al Portal y seguir los pasos señalados para modificar la información proporcionada.
          Procedimiento para ejercer derechos de Acceso, Rectificación y Cancelación de sus datos personales: En caso que el Usuario desee ejercer sus derechos de acceso, rectificación, cancelación u oposición, estos los podrán ejercer en todo
          momento dirigiendo su solicitud a (i) al correo electrónico contacto@klugger.mx Para lo anterior, deberá hacer del conocimiento de "Klugger" de saber fehacientemente los datos personales que el Usuario desea sean rectificados,
          cancelados o revisados, así como el propósito para el cual los aportó y el nombre del Responsable a quien se los entregó y en general cumplir los requisitos mencionados en el art. 29 de la Ley Federal de Protección de Datos Perso-
          nales en Posesión de Particulares.
          Si desea revocar su consentimiento a nuestra Política de Privacidad, póngase en contacto con nosotros a través del formulario "Contáctanos" antes mencionado. Sin embargo, tenga en cuenta que si retira su consentimiento, no podrá usar los servicios correspondientes y serán suprimidos sus datos personales de la cuenta de registro del Sitio Web.
          Si optó por recibir Newsletters, correos electrónicos comerciales u otras comunicaciones de “Klugger” o de terceras partes en el momento de registrarse, pero posteriormente cambia de opinión, podrá anular su opción editando su perfil de cuenta según lo arriba descrito. Si previamente optó por no recibir esas comunicaciones, podrá posteriormente anular esa opción editando su perfil de cuenta.</p>
          <b>6. CAMBIOS EN EL AVISO DE PRIVACIDAD.</b>
          <p>El presente aviso de privacidad, podrá ser actualizado, con la finalidad de reflejar los cambios que se produzcan en los servicios de "Klugger". En dicho supuesto, se modificará la fecha de “última actualización”, en la parte superior de la misma. Si se realizan cambios materiales en este aviso de privacidad, o bien en la forma que "Klugger" usa y/o transmite la información personal, se le notificará con anterioridad a su implementación al usuario, mediante un comunicado al correo electrónico que para tales efectos haya dispuesto.</p>
          <b>7. DERECHO DE PROMOVER LOS PROCEDIMIENTOS DE PROTECCIÓN DE DERECHOS Y VERIFICACIÓN.</b>
          <p>Cualquier queja o información adicional respecto al tratamiento de sus datos personales o duda en relación con la LeyvFederal de Protección de Datos Personales en Posesión de los Particulares o con su Reglamento, podrá dirigirla al Instituto Nacional de Transparencia, Acceso a la Información y Protección de Datos Personales (INAI), con domicilio en Insurgentes Sur No. 3211 Col. Insurgentes Cuicuilco, Delegación Coyoacán, C.P. 04530, teléfono 0 1800 835 4324.</p>
          <b>8. CONTACTO.</b>
          <p>En caso de dudas y/o comentarios a este aviso de privacidad, el usuario podrá contactar a "Klugger", mediante comunicado electrónico al correo contacto@klugger.mx obteniendo una respuesta sobre el mismo, en un plazo de 72 horas hábiles.</p>
          <b>9. MARCO LEGAL VIGENTE.</b>
          <p>El usuario se obliga, por el simple hecho de navegar a través del Portal, a respetar toda la normativa legal vigente relacionada con la utilización, gestión y uso de sistemas de información. "Klugger" extiende el presente aviso de privacidad, de conformidad con lo dispuesto por la Ley Federal de Protección de Datos Personales, así como por las normas aplicables a nivel nacional e internacional.</p>
          <p>Política de privacidad actualizada al 13/10/2015</p>
        </section>
      </section>
    );
  }
}

export default PoliticaPrivacidad;
