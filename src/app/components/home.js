import React from "react";
import { Link } from 'react-router';
import update from 'immutability-helper';
import mapa from '../../media/hero@2x.png';
import logo from '../../media/logo.svg';
// import facebook from '../../media/facebook.svg';
// import instagram from '../../media/instagram.svg';
// import twitter from '../../media/twitter.svg';
import background from '../../media/background2.png';
import mensaje from '../../media/email.svg';
import api from'../utils/api';
import $ from 'jquery';
import {TimelineLite,Sine} from 'gsap';

export class Home extends React.Component {
  constructor(props){
    super(props);
    this.state = {
        contact:{
          hasErrors:false,
          email:"",
          force:false
        }
      }
    }

  handleChange = ( data, event ) => {
    var state = this.state;
    state[ data ] = event.target.value;
    this.setState({
       email: state
    });
    this.setState({
      contact: update(this.state.contact, {
        email: {$set: state.email}
      })
    });
  }

  contact(){
    api.contact( this.state.contact )
    .then((response) => {
      if(response.data.message === "email sent"){
        var tlcontact = new TimelineLite();
        var contactMsg = $(".static--contact-msg");
          tlcontact
          .to(contactMsg, 0.3, {y:"0%", ease:Sine.Elastic})
          .to(contactMsg, 0.3, {y:"100%", delay:4})
      }else{
        this.setState({
          hasErrors: true
        });
      }
    })
  }

  showInput(data) {
    if( data === 'contacto'){
      $("#contacto").fadeIn();
      $("#success").fadeOut();

    }
  }
  resend(){
    this.setState({
      contact: update(this.state.contact, {
        force: {$set: true}
      })
    },() =>
      this.contact()
    );

  }
  optionNo(){
      $("#contacto").fadeOut();
      $("#success").fadeIn();
  }

  render() {
    if(this.state.hasErrors){
      var primer = 'Este correo ya esta registrado';

      var errorSpan = 'Da click aqui si quieres que te enviemos de nuevo información';
      $("#message").fadeIn();
    }

    return (
        <section className="home-nuevo">
          <div className="home-nuevo-contenedor">
            <img src={background} alt="Blue wave background" />
            <div className="header">
              <Link to="/">
                <img src={logo} alt="logo" />
              </Link>
              <a href="mailto:hola@klugger.mx" title="Email">
                <img className="icons icons-2" src={mensaje} alt="Email icon" draggable="false" />
              </a>

            </div>
            <div className="contenedor">
              <div className="home-nuevo-contenido">
                <img src={mapa} alt="" className="mapa"/>
              </div>
              <div className="contenedor-dos">
                <h1>Una nueva experiencia inmobiliaria.</h1>
                <p>Encontramos las mejores propiedades para ti, tomando en cuenta tus gustos, tu estilo de vida y tus preferencias.</p>
                <div>
                  <button className="success" onClick={(e) => this.showInput('contacto')} id="success">COMENZAR</button>
                </div>
                <div className="contacto" id="contacto">
                  <input type="email" placeholder="ejemplo@mail.com" className="border"
                  defaultValue={this.state.email}
                  onChange={(e) => this.handleChange('email', e)}/>
                  <button onClick={(e) => this.contact(this)}>Enviar</button>
                  <div className="message" id="message">
                    <span className="error">{primer}</span>
                    <span onClick={() => this.resend(this)} className="error-click">{errorSpan}</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="static--contact-msg">
            <header>
              <h1>¡Gracias por registrarte!</h1>
              <p style={{'color':'white'}}>Te mandamos un correo con mas información sobre nuestra plataforma.</p>
            </header>
          </div>
        </section>
    );
  }
}

export default Home;
