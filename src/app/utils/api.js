var axios = require('axios');

var property_url = 'https://tnha22duoj.execute-api.us-east-1.amazonaws.com/prod';
module.exports = {

  contact(data){
    console.log("DATA",data);
    var url = property_url + '/admin/landing';
    var encodedURI = window.encodeURI(url);
    var config = {
      headers: {
        'X-My-Custom-Header': 'Header-Value'
      }
    };
    return axios({
      method:'POST',
      url: encodedURI,
      data:{
        email:data.email,
        force:data.force
      },
      config
    })
    .then(function (response) {
      console.log("response");
        return response
    })
    .catch(function (error) {
        return error;
    });
  }

}
