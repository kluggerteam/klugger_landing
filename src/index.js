import React from 'react';
import {render} from 'react-dom';
import {Router, Route, browserHistory, IndexRoute} from "react-router"

import {Root} from "./layout/root";
import {Home} from './app/components/home';
import {TerminosyCondiciones} from './app/static/terminos_condiciones';
import {PoliticaPrivacidad} from './app/static/politica_privacidad';
import {NotFound} from './app/static/not_found';

class App extends React.Component {
  render() {
    return(
      <Router history={browserHistory}>

        <Route path={"/"} component={Root}>
          <IndexRoute component={Home}/>
          <Route path={"terms"} component={TerminosyCondiciones}/>
          <Route path={"privacy"} component={PoliticaPrivacidad}/>
          <Route path={"notFound"} component={NotFound}/>
        </Route>

      </Router>
    );
  }
}

render(<App />, document.getElementById('root'));
