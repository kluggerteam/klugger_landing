import React from 'react';
import { Link } from 'react-router';


export class Footer extends React.Component {
  render() {
    return (
      <footer>
          <hr />
          <div>
            <p>&#169; 2018 Klugger.</p>
            <ul>
              <li><Link to="/privacy" >Privacidad</Link></li>
              <li><Link to="/terms" >Términos</Link></li>
            </ul>
          </div>
      </footer>
    );
  }
}

export default Footer;
