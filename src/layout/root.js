import React from "react";

import Footer from './footer';

export class Root extends React.Component {
  render() {
    return (
        <main style={{'height':'100%'}}>

            {this.props.children}
            <Footer />
        </main>
    );
  }
}

export default Root;
